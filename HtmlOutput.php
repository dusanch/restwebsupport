<?php

/**
 * Class Html
 */
class Html
{


    /**
     * @var array
     */
    private static $columnTypes = ['type', 'prio', 'id', 'name', 'content', 'ttl', 'note'];

    /**
     * Header html
     *
     * @return string
     */
    public static function header() {
        return '<!DOCTYPE html>
                    <html lang="en-US">
                    <head>
                    <title>HTML header Tag</title>
                    <meta charset="utf-8">
                    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
                    <body>';
    }

    /**
     * Footer html
     *
     * @return string
     */
    private static function footer() {
        return '</body></html>';
    }

    /**
     * Generate table
     *
     * @param array $JsonDatas json datas
     * @return string
     */
    public static function getTable($JsonDatas) {

        $table = self::header();
        $table .= '<a href="index.php?t=addnew&type=A">Add new A</a><br>';
        $table .= '<a href="index.php?t=addnew&type=MX">Add new MX</a><br>';
        $table .= '<table border="1"><tr>';
        foreach (self::$columnTypes as $type) {
            $table .= "<td>" . $type . "</td>";
        }
        $table .= "<td></td></tr>";

        foreach ($JsonDatas as $val) {

            if ($val->type == 'MX' || $val->type == 'A') {
                $table .= '<tr>';
                foreach (self::$columnTypes as $key) {
                    $table .= '<td>' . @$val->$key . '</td>';
                }
                $table .= '<td><a href="index.php?t=delete&id=' . $val->id . '">Delete</a></td>';
                $table .= '</tr>';
            }
        }

        $table .= '</table>';
        $table .= self::footer();

        return $table;
    }

    /**
     * Build html form
     *
     * @param string $type typ: A or MX
     * @return string
     */
    public static function form($type = 'A') {
        $form = self::header();

        $form .= '<form method="post" id="addRecord" action="index.php?t=add">';
        $form .= '<label>Type:</label> ';
        $form .= '<input type="text" name="type" readonly="readonly" value="' . $type . '" /> ';
        $form .= '<label>Name:</label> ';
        $form .= '<input type="text" name="name" /> ';
        $form .= '<label>Content :</label> ';
        $form .= '<input type="text" name="content" /> ';

        if ($type == 'MX') {
            $form .= '<label>Prio :</label> ';
            $form .= '<input type="text" name="prio" /> ';
        }

        $form .= '<label>TTL :</label> ';
        $form .= '<input type="text" name="ttl" /> ';
        $form .= '<button type="submit">Add</button>';
        $form .= '</form>';

        $form .= '<script>               
                $("#addRecord").submit(function( event ) {                                      
                    totalEmptyFields = $("input").filter(function () {
                        return $.trim($(this).val()).length == 0
                    }).length;
                                       
                    if (totalEmptyFields>0) {
                        alert("form is empty");
                        return false;
                    } else {
                        return true;
                    }
                });
                
        </script>';

        $form .= self::footer();
        return $form;
    }
}