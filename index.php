<?php

include 'config.php';
include "RestConnection.php";
include "HtmlOutput.php";

$api = new RestConnection(REST_API_URL, REST_API_KEY, REST_SECRET);

$task = $_REQUEST['t'] ?? '';

if ($task == 'add') {
    $response = $api->put(REST_PATH_GET, $_POST);
    header('Location: index.php?s=' . $response->status);
    exit();
}

if ($task == 'delete') {
    $id = intval($_REQUEST['id']);
    $response = $api->delete(REST_PATH_GET . '/' . $id);
    header('Location: index.php?s=' . $response->status);
    exit();
}


if ($task == 'addnew') {

    $type = $_REQUEST['type'] ?? 'A';

    echo Html::form($type);
}

if ($task=='') {
    $response = $api->get(REST_PATH_GET);
    echo Html::getTable($response->items);
}






