<?php


class RestConnection
{

    private $url;
    private $apiKey;
    private $apiSecret;


    /**
     * RestConnection constructor.
     * @param $url
     * @param null $apiKey
     * @param null $apiSecret
     */
    public function __construct($url, $apiKey = null, $apiSecret = null) {
        $this->url = $url;
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }


    private function getConnection() {
        return curl_init();
    }


    /**
     * @param $url
     * @param null $params
     * @return mixed
     */
    public function get($url, $params = null) {
        list ($body, $responseCode) = $this->doRequest('GET', $url, $params !== null ? json_encode($params) : null);

        if ($responseCode === 200 || $responseCode === 201 || $responseCode === 422 || $responseCode === 400) {
            return $body;
        } else {
            die($body->message);
        }
    }

    /**
     * @param $url
     * @param null $params
     * @return mixed
     */
    public function put($url, $params = null) {

        list ($body, $responseCode) = $this->doRequest('POST', $url, $params !== null ? json_encode($params) : null);

        if ($responseCode === 200 || $responseCode === 201 || $responseCode === 422 || $responseCode === 400) {

            if ($body->status=='error') {

                if (!empty($body->errors->content)) {
                    foreach ($body->errors->content as $item) {
                        echo $item . '<br>';
                    }
                }
                die();
            }
            return $body;
        } else {
            die($body->message);
        }
    }

    /**
     * @param $url
     * @param null $params
     * @return mixed
     */
    public function delete($url, $params = null) {
        list ($body, $responseCode) = $this->doRequest('DELETE', $url, $params !== null ? json_encode($params) : null);

        if ($responseCode === 200 || $responseCode === 201 || $responseCode === 422 || $responseCode === 400) {
            return $body;
        } else {
            die($body->message);
        }
    }

    /**
     * @param string $request request GET POST
     * @param string $path path
     * @param null $requestBody
     * @return array
     */
    protected function doRequest($request, $path, $requestBody = null) {

        $time = time();
        $canonicalRequest = sprintf('%s %s %s', $request, $path, $time);
        $signature = hash_hmac('sha1', $canonicalRequest, $this->apiSecret);

        $ch = $this->getConnection();

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request);
        curl_setopt($ch, CURLOPT_URL, sprintf('%s:%s', $this->url, $path));

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->apiKey . ':' . $signature);

        if (!empty($requestBody)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);  // Post Fields
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Date: ' . gmdate('Ymd\THis\Z', $time),
        ]);

        $response = curl_exec($ch);
        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return array(json_decode($response), $httpStatus);

    }


}